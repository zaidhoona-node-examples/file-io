/**
 * Main file (point where the execution starts)
 *
 * @author Zaid Hoona
 */

var fs = require("fs");
var path = require("path");

var noteFilePath = path.resolve("assets", "note.txt");
console.log(readFileSync(noteFilePath));

readFileAsync(noteFilePath, function(data) {
  console.log("\n\nAsync file.");
  console.log(data);
});

/**
 * Reads file synchronously
 *
 * @param filePath path to the file to read
 */
function readFileSync(filePath) {
  return fs.readFileSync(filePath).toString();
}

/**
 * Reads file asynchronously
 *
 * @param filePath path to the file to read
 * @param callback function to call after reading the file
 */
function readFileAsync(filePath, callback) {
  return fs.readFile(filePath, null, function(err, buffer) {
    if (err) {
      console.log(err);
    }

    callback(buffer.toString());
  });
}
